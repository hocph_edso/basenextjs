const Footer = () => {
  const policy  = [
    {
      name: "Quyền riêng tư",
      url: "/"
    },
    {
      name: "Điều khoản",
      url: "/"
    },
    {
      name: "Sơ đồ trang web",
      url: "/"
    }
  ]
  return (
    <footer className="footer bg-gray-100">
      <div className="container mx-auto">
        <div className="py-10"></div>
        <div className="copyright flex items-center space-x-3 text-sm border-t border-gray-300 py-6">
          <div className="copyright-text">© 2021 EdsoLabs, Inc.</div>
          <div className="flex items-center space-x-3">
            {policy.map((item, index) => (
              <div key={index} className="list-circle pl-3">
                <a href="{item.url}">{item.name}</a>
              </div>
            ))}
          </div>
        </div>
      </div>
    </footer>
  )
}

export default Footer
