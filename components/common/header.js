import Link from 'next/link'

const Header = () => {
  return (
    <header className="header bg-gray-500 text-black">
      <div className="container mx-auto flex items-center">
        <div className="logo">
          <Link href="/">
            <img src="./logo.png" alt="logo" className="w-10 h-auto cursor-pointer" />
          </Link>  
        </div>
        <div className="header-right flex items-center ml-auto">
          <div className="h-10 flex justify-center items-center text-sm font-semibold px-6 cursor-pointer text-white rounded-full hover:bg-gray-300 hover:bg-opacity-10">
            <Link href="/chu-nha">Trở thành chủ nhà</Link>
          </div>
        </div>
      </div>
    </header>
  )
}

export default Header
