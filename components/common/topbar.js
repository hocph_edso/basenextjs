const Topbar = () => {
  return (
    <div className="bg-black">
      <div className="container mx-auto py-4 text-sm text-center text-white cursor-pointer hover:text-opacity-80 underline">
        Nhận thông tin mới nhất về biện pháp ứng phó COVID-19 của chúng tôi
      </div>
    </div>
  )
}

export default Topbar
