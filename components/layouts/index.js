import Head from 'next/head'
import Header from '../common/header'
import Footer from '../common/footer'
import TopBar from '../common/topbar'

const LayoutDefault = ({ children }) => {
  return (
    <>
      <Head>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>Project Clone Airbnb for nextjs</title>
      </Head>
      <TopBar />
      <Header />
      <div className="page-content container mx-auto">{children}</div>
      <Footer />
    </>
  )
}

export default LayoutDefault
