import PostItem from './PostItem'

const ListPost = ({ lists, title, widthCol }) => {
  return (
    <div>
      <div className="text-2xl font-bold mb-3">{title}</div>
      <div className="flex space-x-4">
        {lists.map((item, index) => (
          <PostItem key={index} data={item} widthCol={widthCol} />
        ))}
      </div>
    </div>
  )
}

export default ListPost