import Image from 'next/image'

const PostItem = ({data, widthCol}) => {
  const classWidth = 'w-1/' + widthCol
  return (
    <div className={`post-item ${classWidth}`}>
      <div className="post-item__image mb-2 rounded-lg overflow-hidden relative pt-full">
        <Image 
          src={data.image} alt={data.title} 
          layout="fill"
          objectFit="cover"
          quality={100}
        />
      </div>
      <div className="post-item__title font-bold text-lg">{data.title}</div>
      <div className="post-item__desc text-sm">{data.description}</div>
    </div>
  )
}

export default PostItem