import Image from 'next/image'

const Banner = ({data}) => {
  return (
    <div className="relative rounded-lg overflow-hidden h-96 flex items-center text-white">
      <div className="banner-content z-10 relative pl-20 max-w-lg">
        <div className="banner__title text-4xl font-bold mb-3">{data.title}</div>
        <div className="banner__desc text-lg">{data.desc}</div>
        <div className="mt-8 flex justify-start cursor-pointer">
          <div className="rounded-lg bg-white h-12 flex items-center justify-center text-black font-bold px-6">Tìm hiểu thêm</div>
        </div>
      </div>
      <Image 
        src={data.image} alt={data.title} 
        layout="fill"
        objectFit="cover"
        quality={100}
      />
    </div>
  )
}

export default Banner