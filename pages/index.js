import Head from 'next/head'
import dynamic from 'next/dynamic'

const Banner = dynamic(() => import('../components/banner/Banner'))
const ListPost = dynamic(() => import('../components/post/ListPost'))

const HomePage = () =>  {
  const banner = {
    title: 'Thử đón tiếp khách',
    desc: 'Kiếm thêm thu nhập và khám phá các cơ hội mới bằng cách chia sẻ nơi ở của bạn.',
    image: '/banner/banner-1.webp'
  }
  const listPost = [
    {
      id: 1,
      title: 'Nơi nghỉ dưỡng ngoài trời',
      image: '/post/post-4.webp',
      url: '',
    },
    {
      id: 2,
      title: 'Chỗ ở độc đáo',
      image: '/post/post-5.webp',
      url: '',
    },
    {
      id: 2,
      title: 'Toàn bộ nhà',
      image: '/post/post-6.webp',
      url: '',
    },
    {
      id: 2,
      title: 'Cho phép mang theo thú cưng',
      image: '/post/post-7.webp',
      url: '',
    }
  ]
  const listPost1 = [
    {
      id: 1,
      title: 'Trải nghiệm',
      image: '/post/post-1.webp',
      url: '',
      description: 'Tìm các hoạt động khó quên gần bạn.'
    },
    {
      id: 2,
      title: 'Trải nghiệm trực tuyến',
      image: '/post/post-2.webp',
      url: '',
      description: 'Các hoạt động tương tác, truyền trực tiếp dưới sự dẫn dắt của Người tổ chức.'
    },
    {
      id: 2,
      title: 'Bộ sưu tập nổi bật: Phiêu du',
      image: '/post/post-3.webp',
      url: '',
      description: 'Du lịch tại nhà với Trải nghiệm trực tuyến.'
    }
  ]
  return (
    <>
    <Head>
      <title>Trang chủ</title>
    </Head>
    <div className="py-4 space-y-14">
      <ListPost lists={listPost} title="Ở bất cứ đâu" widthCol="4" />
      <Banner data={banner}></Banner>
      <ListPost lists={listPost1} title="Khám phá những điều nên trải nghiệm" widthCol="3" />
    </div>
    </>
  )
}

export default HomePage
